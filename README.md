# Anitweaks

<p style="text-align: center;">
	A collection of tweaks for Anilist.
</p>

<p align="center">
    <a href="https://gitlab.com/wolfiy/anitweaks/-/raw/master/anitweaks.user.css" alt="install black AL with stylus">
        <img src="https://img.shields.io/badge/Install%20with-Stylus-blue.svg" /></a>
</p>

## Note

This project is still in its early days, more to come soon!

## Features

- Hide some or all activities
- Various visual tweaks

## Installation

There are a few ways to install this style.

### Using Stylus (recommended)

 1. Install Stylus ([Firefox](https://addons.mozilla.org/en-US/firefox/addon/styl-us/), [Chrome](https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne?hl=en), [Opera](https://addons.opera.com/en/extensions/details/stylus/)).

 2. Follow [this link](https://gitlab.com/wolfiy/anitweaks/-/raw/master/anitweaks.user.css).

 3. Confirm the installation.

### Using Stylus (manual install)

If the previous method won't work, you can do the following.

 1. Install Stylus ([Firefox](https://addons.mozilla.org/en-US/firefox/addon/styl-us/), [Chrome](https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne?hl=en), [Opera](https://addons.opera.com/en/extensions/details/stylus/)).

 2. Follow [this link](https://gitlab.com/wolfiy/anitweaks/-/raw/master/anitweaks.user.css) and copy everything (`ctrl+a, ctrl+c` or `cmd+a, cmd+c`).

 3. Go to [Anilist](https://anilist.co/home) and click the Stylus icon on your browser.

 4. Under "Write style for", click "anilist.co". It will open a code editor.

 5. Paste the content from the previous link (`ctrl+v` or `cmd+v`).

 6. Hit save.

### Using Cascadea (Safari)

 1. Install [cascadea](https://apps.apple.com/app/cascadea/id1432182561).

 2. Follow the same instruction as for Stylus.

Note: if you wish to change the accent color, you might need to manually set it in the code!

### Note

There is an older extension, *Stylish*, that I **do not** recommend using; it has been proved to be spyware and is very outdated.

## FAQ

### Can I try new features?

Yes! Switch the the [dev branch] and follow the same instructions. Be aware that dev releases are prone to be broken and/or incoherant.

### Something is not working properly, can you help me? / I have a suggestion

If you have an account on GitLab, simply open an issue and describe what is going wrong. Otherwise, you can pm me on [Anilist](https://anilist.co/user/137739/) or on Discord, `wolfiy.#0405`. Please specify what browser and OS you are using!

### I cannot / do not want to use Stylus. Are there any alternatives?

Yes, see [this page](https://github.com/openstyles/stylus/wiki/Stylish-alternatives).