# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.3] - 2023-03-23

## Added

- Option to hide like count
- (Unfinished) compact mode

## [0.0.2] - 2023-03-22

### Added

- Readme

## [0.0.1] - 2023-03-22

### Added

- Began the project
- Changelog
- License
- Toggle banner shadows
- Hide some or all activities
- Logo hue selector